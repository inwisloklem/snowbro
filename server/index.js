const cors = require('cors')
const express = require('express')
const app = express()

const answers = require('./data/answers')
const prettyPrint = require('./helpers/prettyPrint')
const randomAnswer = require('./helpers/randomAnswer')
const settings = require('./settings')

app.use(cors())
app.options('*', cors())
app.use(express.json())

// Mock API
app.post(settings.AUTH, (req, res) => {
  console.info(`Request:\n${prettyPrint(req.body)}`)

  res.setHeader('Content-Type', 'application/json')
  res.json({
    'data': {
      'type': 'HotspotAuth',
      'attributes': {
        'id': 'b54adc00-67f9-11d9-9669-0800200c9a66',
        'phrases': randomAnswer(answers)
      }
    }
  })
})

app.post(settings.CHECK, (req, res) => {
  console.info(`Request:\n${prettyPrint(req.body)}`)

  res.setHeader('Content-Type', 'application/json')
  res.json({
    'data': {
      'type': 'HotspotAuthCheck',
      'attributes': {
        'success': true,
        'phrase': 'Снегу быть – Обязательно быть'
      }
    }
  })
})

app.listen(settings.PORT)
