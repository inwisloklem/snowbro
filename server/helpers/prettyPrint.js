function prettyPrint (json) {
  return JSON.stringify(json, null, 2)
}

module.exports = prettyPrint
