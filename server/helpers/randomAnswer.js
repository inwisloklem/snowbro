function randomAnswer (arr) {
  const shuffled = arr.sort(() => Math.random() > 0.5)
  return shuffled[0]
}

module.exports = randomAnswer
