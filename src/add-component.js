import VueScrollTo from 'vue-scrollto'
import settings from './settings'

const {
  AVATAR_COMPONENTS,
  SCROLL_ANIMATION_TIME,
  USER_TRIANGLE_COMPONENTS
} = settings

// Switches bot avatar and triangle
function setAvatar ({ component, queue }) {
  if (AVATAR_COMPONENTS.includes(component.type)) {
    const lastAvatarComponent = queue.find(component => component.avatar === true)

    if (lastAvatarComponent) {
      lastAvatarComponent.avatar = false
    }
    component.avatar = true
  }
}

// Switches user triangle
function setUserTriangle ({ component, queue }) {
  if (USER_TRIANGLE_COMPONENTS.includes(component.type)) {
    const lastTriangleComponent = queue.find(component => component.userTriangle === true)

    if (lastTriangleComponent) {
      lastTriangleComponent.userTriangle = false
    }
    component.userTriangle = true
  }
}

function addComponent ({ queue, ms, component }) {
  const { documentElement } = document

  if (ms) {
    return new Promise(resolve => {
      setTimeout(() => {
        setAvatar({ component, queue })
        setUserTriangle({ component, queue })
        queue.push(component)
        VueScrollTo.scrollTo(documentElement, SCROLL_ANIMATION_TIME, {
          offset: documentElement.scrollHeight
        })
        resolve()
      }, ms)
    })
  }

  return new Promise(resolve => {
    setAvatar({ component, queue })
    setUserTriangle({ component, queue })
    queue.push(component)
    VueScrollTo.scrollTo(documentElement, SCROLL_ANIMATION_TIME, {
      offset: documentElement.scrollHeight
    })
    resolve()
  })
}

export default addComponent
