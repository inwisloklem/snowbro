export default {
  ANIMATION_TIME: 500,
  NEXT_MESSAGE_FROM: 300,
  NEXT_MESSAGE_TO: 500,
  SCROLL_ANIMATION_TIME: 200,
  TELEGRAM_LINK: 'https://t.me/SnowBroBot',
  WIFI_LAW_LINK: 'https://global-hotspot.ru/штраф-закон-о-wi-fi/',
  PHONE_NUMBER_LENGTH: 11,
  AVATAR_COMPONENTS: [
    'bot-bubble'
  ],
  USER_TRIANGLE_COMPONENTS: [
    'user-bubble'
  ],
  BASE_API_URL: '//localhost:3000/backend/hotspot/auth',
  REQUEST_TIMEOUT: 5000
}
