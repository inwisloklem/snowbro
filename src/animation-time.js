import settings from './settings'

const {
  ANIMATION_TIME,
  NEXT_MESSAGE_FROM,
  NEXT_MESSAGE_TO
} = settings

function getRandomInt (min, max) {
  return Math.floor(Math.random() * (max - min)) + min
}

function getAnimationTime () {
  return ANIMATION_TIME + getRandomInt(NEXT_MESSAGE_FROM, NEXT_MESSAGE_TO)
}

export default getAnimationTime
